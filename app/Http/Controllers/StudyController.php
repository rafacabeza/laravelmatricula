<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\StudyRequest;
use App\Study;

class StudyController extends Controller
{
    //
    public function index()
    {
        $studies = \App\Study::all();
        // dd($studies);
        return view('study.index', ['studies' => $studies]);
    }    //
    public function create()
    {
        // dd($studies);
        return view('study.create   ');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $this->validate($request, [
            'code' => 'required|unique:studies',
            'abreviation' => 'required|unique:studies',
            'name' => 'required',
            'shortName' => 'required'
            ]);

            // 'name' => 'required|unique:posts|max:255',

        $study = new Study($request->all());
        // dd($study);
        $study->save();

        return redirect('/study');
    }

    public function delete($id)
    {
        // modo 1
        // $study = Study::find($id);
        // $study->delete();
        // modo 2
        Study::destroy($id);

        return redirect('/study');

    }

    public function update($id)
    {
        $study = Study::find($id);
        return view('study.update', ['study' => $study]);
        // dd($study);
    }

    public function save(StudyRequest $request)
    {
        // $id = $request['id'];
        // dd($request);
        $study = Study::find($request->id);
        $study->code = $request->code;
        $study->name = $request->name;
        $study->shortName = $request->shortName;
        $study->abreviation = $request->abreviation;
        $study->save();
        return redirect('/study');
    }


    //ajax
    /** Métodos usados con Ajax. Devuelven JSON **/
    public function ajax()
    {
        return view('study.ajax');
    }

    public function ajaxlist()
    {
        $studies = \App\Study::all();
        // return $studies;
        return response()->json($studies);
    }

    public function ajaxlist2()
    {
        $studies = \App\Study::all();
        return  $studies;
    }
}

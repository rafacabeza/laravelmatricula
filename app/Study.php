<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Study extends Model
{
    protected $fillable = ['code', 'name', 'shortName', 'abreviation'];
}

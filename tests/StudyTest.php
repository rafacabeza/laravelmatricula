<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StudyTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */

    //si usamos este trait hacemos rollback a todas las migraciones
    //por tanto dejamos la bbdd vacía.
    //use DatabaseMigrations; 


    public function testList()
    {
        $this->visit('/study')
            ->see('Lista de estudios');
    }

    public function testCreate()
    {
        //validar campos obligatorios en create
        $this->visit('/study')
            ->see('Lista de estudios')
            ->see('Nuevo')
            ->click('Nuevo')
            ->seePageIs('/study/create')
            ->see('Alta de estudios')
            ->see('Enviar')
            ->press('Enviar')
            ->seePageIs('/study/create')
            ->see('El campo código es obligatorio')
            ->see('El campo nombre es obligatorio')
            ->see('El campo nombre corto es obligatorio')
            ->see('El campo abreviatura es obligatorio')
            ;

        //crear registros
        $this->visit('/study')
            ->see('Lista de estudios')
            ->see('Nuevo')
            ->click('Nuevo')
            ->seePageIs('/study/create')
            ->see('Alta de estudios')
            ->see('Enviar')
            ->type('IFC303', 'code')
            ->type('Técnico Superior en Desarrollo de aplicaciones Web', 'name')
            ->type('Desarrollo de aplicaciones Web', 'shortName')
            ->type('DAW', 'abreviation')
            ->press('Enviar')
            ->seePageIs('/study')
            ->see('IFC303')
            ->seeInDatabase('studies', ['code' => 'IFC303'])
             ;
    }
}

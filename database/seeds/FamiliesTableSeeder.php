<?php

use Illuminate\Database\Seeder;

class FamiliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('families')->insert([
            'code' => 'SEC',
            'name' => 'Secundaria',
        ]);
        
        DB::table('families')->insert([
            'code' => 'IFC',
            'name' => 'Informática y Comunicaciones',
        ]);
    }
}

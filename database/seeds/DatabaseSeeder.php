<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FamiliesTableSeeder::class);

        $families = factory(App\Family::class, 10)->create();
    }
}

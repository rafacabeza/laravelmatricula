<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="/css/default.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.0/jquery.js"></script>


    <title>@yield('title')</title>
</head>
<body>
    <div id="header">
        <div id="title">@yield('title')</div>
        <div>
            <div style="float: left;">
                <a href="/">Inicio</a>
                <a href="/user">Usuarios</a>
                <a href="/study">Estudios</a>
            </div>
            <div style="float: right;">
                <a href="/login">login</a>
            </div>
        </div>
    </div>

    <div id="content">
    @section('content')
    <h1>Cuerpo de la vista</h1>


    @show

    </div>

    <div id="footer"> Pie de página</div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.js"></script>

    @yield('scripts')

    </body>
</html>
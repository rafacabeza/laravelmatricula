<!DOCTYPE html>
<html>
<head>
    <title>User</title>
    <style>
        table {border:2px solid black; border-collapse:collapse;}
        td {border:1px solid black; }
    </style>
    </head>
    <body>
        <h1>Lista de usuarios</h1>
        <table>
            @foreach ($users as $id=>$user)
            <tr>
                <td>{{ $id }} </td>
                <td>{{ $user }} </td>
            </tr>
            @endforeach
        </table>
    </body>
    </html>
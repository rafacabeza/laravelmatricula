@extends('layouts.class')

@section('title', 'Matrícula con Laravel')

@section('content')
    <h1>Lista de estudios</h1>

    <p><a href="/study/create">Nuevo</a></p>
    <table>
        <tr>
            <th>Id</th>
            <th>Cod.</th>
            <th>Abrev.</th>
            <th>Nombre Corto</th>
            <th>Título</th>
            <th></th>
        </tr>

        @foreach($studies as $study)
        <tr>
        <!-- observa que podemos recuperar atributos de dos modos: -->
            <td>{{ $study['id'] }}</td>
            <td>{{ $study['code'] }}</td>
            <td>{{ $study['abreviation'] }}</td>
            <td>{{ $study->shortName }}</td>
            <td>{{ $study->name }}</td>
            <td>
                <a href="/study/update/{{ $study['id'] }}">Editar</a>
                <a href="/study/delete/{{ $study->id }}">Borrar</a>
            </td>
        </tr>
        @endforeach
        </table>

    <p><a href="/study/create">Nuevo</a></p>
        
@stop


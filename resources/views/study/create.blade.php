@extends('layouts.class')

@section('title', 'Matrícula con Laravel')

@section('content')
    <h1>Alta de estudios</h1>

    <form method="post" action="/study/create">
         {{ csrf_field() }}
         <div>
         <label>Código</label>
         <input type="text" name="code" value="{{ old('code') }}">
        {{ $errors->first('code') }}
        </div>

        <div>
         <label>Nombre</label>
         <input type="text" name="name" value="{{ old('name') }}">
         {{ $errors->first('name') }}
         </div>

         <div>
         <label>Nombre Corto</label>
         <input type="text" name="shortName" value="{{ old('shortName') }}">
         {{ $errors->first('shortName') }}
          </div>

        <div>
        <label>Abreviatura</label>
        <input type="text" name="abreviation" value="{{ old('abreviation') }}">
         {{ $errors->first('abreviation') }}
        </div>
        <label></label>
        <input type="submit" value="Enviar"><br>
    </form>

    <ul>
    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
    </ul>
@stop
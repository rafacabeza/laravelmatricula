$(document).ready(function () {
//.........................................................................
//            Limpieza de los input de nuevo registro
//.........................................................................
    var clearInputNew = function() {
        $('#codigoNew').val('');
        $('#nombreNew').val('');
        $('#existenciaNew').val('');
        $('#precioNew').val('');
    };
    
    var saveCell = function(input) {
        //si no hay cambios fuera
        if($(input).attr('oldValue') === $(input).attr("value")){
            exit();
        }  
        
        //tomar valores a salvar
        var idRow = $(input).parent().parent().attr("idRow");
        var column = $(input).attr("id");
        var value = $(input).attr("value");
        var oldValue = $(input).attr("oldValue");

        //llamada ajax -----------------------------------------------
        //guardar cambio en registro
        //------------------------------------------------------------
        var url = 'http://localhost/shop2016/product/ajaxUpdateCell';
        $.post(url, 
            {id: idRow,  
            campo: column,  
            value: value 
            }, 
            function (data) {
                if(!data){                    
                    $(input).attr("value", oldValue);                    
                }
                else{
                    $(input).attr("value", data);                    
                }
            //dejar el estilo por defecto     
            $(input).removeAttr("style");   
        }, 'json');    
    };
    
//.........................................................................
//            Para borrar
//.........................................................................
    
    var newProduct = function () {
//        $.post('http://localhost/proyectotema7/product/ajaxInsertProduct', 'codigo=' + $("#codigo").val(), function (data) {
//            
////            $("#rowSpan").html('');
//            alert(data);
////            exit();
//
//        }, 'html');

    };


//.........................................................................
//                       Carga de página de datos
//                       llamada ajax 
//.........................................................................

    var loadPage = function (numero) {
        url = 'http://localhost/shop2016/product/ajaxPageData/' + numero;
        $.get(url, "", function(data){
            page = data[0];
            pages = data[1];
            rows =  data[2];
            $("#tbodyList").empty();
            filas = "";
            for (var i = 0; i < rows.length; i++) {

                filas = filas + i;
                newRows = '<tr class="cell" id="row' + rows[i].id + '" idRow=' + rows[i].id + ' >';
                newRows = newRows + '<td  class="cell"> <input id="codigo" type="text" class="cell cellInput" value=' + rows[i].codigo + '></td>';
                newRows = newRows + '<td class="cell"><input id="nombre" type="text" class="cell cellInput" value=' + rows[i].nombre + '></td>';
                newRows = newRows + '<td class="cell"> <input id="precio"  type="text" class="cell cellInput" value=' + rows[i].precio + '></td>';
                newRows = newRows + '<td class="cell"> <input id="existencia" type="text" class="cell cellInput" value=' + rows[i].existencia + '></td>';
                newRows = newRows + '<td id="delete' + rows[i].id + '" class="link"> <a>Borrar</a> </td></tr>';
                $(document).on('click', '#delete' + rows[i].id,function() {
                    var idDelete = $(this).attr("id");
                    deleteRecord(idDelete.substring(6));
                });
                
//                Toner Impresora X
                $("#tbodyList").append(newRows);                              
            }


            //construccion del indice de páginas
            var index;
            $("#pageIndex").empty();
            
            //añadimos un elemento por página
//            var min = 0;
            var sizeIndex = 5
            var min = 1;
            var max = pages;
            if(numero - sizeIndex <0) min = 1;
            if (numero +  sizeIndex > pages) max = pages;
            
            min = (parseInt(numero) - sizeIndex < 1)? 1 : parseInt(numero) - sizeIndex;
            max = (parseInt(numero) + sizeIndex > pages)? pages : parseInt(numero) + sizeIndex;

//            $("#pageIndex").append(min + '-' + numero +  '-' + max + '/pages: ' + pages + '  ');
            for (i=min; i <= max; i++){
                    if(numero==i){
                        var classSpan = 'class="current"';
                    }
                    else{
                        classSpan = '';
                    }
                    index = '<span ' + classSpan + ' position="' + i + '" id="index' + i + '">' + i +  '</span>  ' ;
                $("#pageIndex").append(index);
                
                //añadir funcion onclick para paginacion
                if(page!==pages){
                    $(document).on('click', '#index' + i,function() {
                        var pos = $(this).attr("position");
                        loadPage(pos);                 
                    });
                }

            }


           //añadir event handlers para actualizar datos:
           //focusin, keyup, focusup
           
           //guardamos valor viejo para comparar si hay cambios
            $(document.body).on("focusin", ".cellInput", function(){
                var oldVAlue = $(this).val();
                $(this).attr("oldValue", oldVAlue);
            });
            
            //modificamos estilo si cambiamos, o quitamos el estilo
            $(document.body).on("keyup", ".cellInput", function(){
                if($(this).attr('oldValue') != $(this).val()){                                            
                    $(this).css("background-color","orange");                    
                }
                else{
                    $(this).removeAttr("style");   
                }
                
            });
            
            
            //guardamos al salir
            $(document.body).on("focusout", ".cellInput", function(){
                saveCell(this);               
            })            

        }, 'json');

    };
    

//.........................................................................
//                       Borrar registro
//.........................................................................

    var deleteRecord = function (id) {
//        alert (id);
        url = 'http://localhost/shop2016/product/ajaxDelete/' + id;
        $.post(url, "", function (data) {
            if(data){
                $('#row' + id).empty();
            }
            else{
                alert('Operación denegada');
            };
        }, 'json');
    };

//.........................................................................
//                       Funcion aviso de prueba
//.........................................................................

    var aviso = function (numero) {
        alert (numero);
    };

//.........................................................................
//                       Nuevo registro
//.........................................................................

    $("#new").click(function () {
//        alert('Aquí debemos enviar el ajax de insertar registro');
//alert(1);
        url = 'http://localhost/shop2016/product/ajaxInsert/';
        $.post(url, 
            {codigo: $(codigoNew).val(),  
                nombre: $(nombreNew).val(),  
                precio: $(precioNew).val(),  
                existencia: $(existenciaNew).val() 
            }, 
            function (data) {
//                alert(data.toString() );
                if(data){
//                    alert(3);
                     loadPage(1);
                     clearInputNew();
                }
                else alert('Operación denegada');
        }, 'json');
//        newProduct();
    });



//.........................................................................
//                       Ejecución inicial
//.........................................................................

    loadPage(1);
});
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

//rutas de prueba
Route::get('user', function () {
    return 'hola mundo. Estamos en user';
});
Route::post('user', function () {
    return 'hola mundo. Estamos en user, por el método POST';
});

//Pruebas: usando parámetros
Route::get('user/list', function () {
    //return 'Preguntando por el usuario ' . $id;
    $users = array('pepe', 'juan', 'sonia' );
    //dd($users);

    return view('user.list')->with('users', $users);
});

Route::get('user/{id}', function ($id) {
    //return 'Preguntando por el usuario ' . $id;
    return view('user.index', [
        'id' => $id,
        'greeting' => '<h1> hola </h1>'
        ]);
});


//rutas de Estudio-->Study
Route::get('study', 'StudyController@index');
Route::get('study/create', 'StudyController@create');
Route::post('study/create', 'StudyController@store');
Route::post('study/update', 'StudyController@save');
Route::get('study/update/{id}', 'StudyController@update');
Route::get('study/delete/{id}', 'StudyController@delete');
//rutas de Estudio con ajax
Route::get('study/ajax', 'StudyController@ajax');
Route::get('study/ajaxlist', 'StudyController@ajaxList');
Route::get('study/ajaxlist2', 'StudyController@ajaxList2');
Route::post('study/ajaxinsert', 'StudyController@ajaxInsert');

Route::resource('module', 'ModuleController');

Auth::routes();

Route::get('/home', 'HomeController@index');




//Ejemplos de reenvio de respuesta
Route::get('usuario', function () {
    $user = Auth::user();
    echo $user['email'];
    dd($user);
});

Route::get('teruel', function () {
    return redirect('/destino/Teruel');
});

Route::get('zaragoza', function () {
    return redirect()->route('destino', ['sitio' => 'Zaragoza']);
});

Route::get('destino/{sitio}', function ($sitio) {
    return "Destino: $sitio";
})->name('destino');


//Ejemplos de abort
Route::get('aborto', function () {
    abort(404, "Acceso prohibido");
});


//Ejemplos middleware ifAuth
Route::get('invitado', function () {
    return "eres un invitado";
})->middleware('guest');

//Ejemplos middleware ifAuth
Route::get('privado', function () {
    return "zona privada";
})->middleware('auth');
